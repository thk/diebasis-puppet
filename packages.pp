# TODO consider: sysdig, atop, htop, dstat, iotop
$common_packages = [
  'aptitude',
  'etckeeper', # version-control the /etc folder
  'fail2ban', # block IPs after too many failed login attempts
  'git',
  'htop',
  'tmux',
  'vcsh',
]

package { $common_packages:
  ensure => installed,
}
