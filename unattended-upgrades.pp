# Automatically install security updates

# dependency of unattended-upgrades to monitor for shutdown event
package { 'python3-gi':
  ensure => installed,
}

package { 'unattended-upgrades':
  ensure => installed,
}

# Origins-Pattern: Also enable updates for backports!
file { '/etc/apt/apt.conf.d/51puppet-unattended-upgrades':
  content => "# This file is managed by puppet. Do not edit!
Unattended-Upgrade::Mail \"root\";
Unattended-Upgrade::Origins-Pattern {
    \"origin=*\";
};
APT::Periodic::Update-Package-Lists \"1\";
APT::Periodic::Unattended-Upgrade \"1\";
"
}
