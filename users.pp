file { '/etc/sudoers.d/90puppet-nopass':
  content => '%sudo   ALL=(ALL:ALL) NOPASSWD:ALL'
}

# Make sure there is no password set for the root user
user { 'root':
  ensure => present,
  uid => '0',
  home => '/root',
  shell => '/bin/bash',
  password => '*', # see man shadow for passwordless logins
}

package { 'zsh':
  ensure => installed,
}

define diebasis_user (
  String $username,
  String $shell = '/bin/bash',
  Enum['present', 'absent', 'locked'] $state = 'present',
  String $uid,
  Boolean $sudo = true,
  String $ssh_key = '',
  String $ssh_type = 'ssh-rsa',
  String $ssh_title = $username,
  Array[String] $groups = [],
) {
  if $sudo {
    $_groups = $groups << 'sudo'
  } else {
    $_groups = $groups
  }

  case $state {
    'present': {
      $ensure = 'present'
      $password = '*'
    }
    'absent': {
      $ensure = 'absent'
      $password = ''
    }
    locked: {
      $ensure = 'present'
      $password = '!'
    }
  }

  user { $username:
    ensure => $ensure,
    uid => $uid,
    home => "/home/$username",
    shell => $shell,
    gid => 'users',
    groups => $_groups,
    managehome => true,
    password => $password,
  }

  if $state == 'present' {
    ssh_authorized_key { $ssh_title:
      user => $username,
      type => $ssh_type,
      key  => $ssh_key,
    }
  }
}

diebasis_user{ 'test':
  username => 'test',
  state => 'absent',
  uid => '9999',
}

diebasis_user { 'thk':
  username => 'thk',
  uid => '10000',
  shell => '/bin/zsh',
  ssh_title => 'x260.20200121',
  ssh_key => 'AAAAB3NzaC1yc2EAAAADAQABAAACAQDAe7rU/LHAs9jrWBYH3Jf9VaD1qenE7n0H8zsSzPIe0dHT9/M782q1nnsefdhq7lq1MBBPowCGyyQxCP9TTGVVXSIVsTe2WXPLXcZlMJ8YdWwD4xUfa6XolohIOF3PG2N1gLDqLcPZkRwLyOZkJyuFx6krKbWLYtbGcer7DldBebSHlW4AENslMNHmsrx6gxBTdfPGQpvkG/TPtfjMD0Mvcx2oHsq7+nIyv7quqyv+6h9jf9IWRYw/t8WAwgwkFMqU+mlMR3ITiwHIm+PzSUDCB3G9+Q8ZQtRS+DhliFFIA6lQQbr+JPpckQ60Jf3vpfnVRj3L5yM0pmDQIHsiPfdAVyJ/hrIrf1609kAcfK8xE60SgiVhOJTSXkL/tnCYAcmmt3r8hLewRd8VgEXhFl7cLayBj8cRLMMmf6QYi+fyCYVKZ+84mW9MP2v/cen5Sui/PzJbIoPxY1P8Rz8tznQxjV3EL1HF8e5As6LMLumXLlmLQk/p89V1P+8aOmCoWRBuq5YjMD3FXtBW2MXaObQULKuMP8K/OfzON0P7B49gsAOsubY2HkxFPLTF5a4LnkF+R5NkDG8qfPMWlqjJjNHOJpLy1KTNxxKdVOGmiij3ve6lAOmBZEPj4gPfZWTkyXcYJ1uMJMbLoGBk49aXe66Kow9F5L9bpYsWyl7xfT4Skw==',
}

diebasis_user{ 'nikolai':
  username => 'nikolai',
  uid => '10001',
  ssh_key => 'AAAAB3NzaC1yc2EAAAABJQAAAQEAuEGG6Uedav9FC/9uMOCuBfmTCpQheZNPQui+0J8Hk3UqxHamHL9StaKQrER+0oyXDhvIxzrnrZin5PGwWp9oKBOF/PLj5Ba3oWrskNAMKax1g0I33RYSk1AgRCx6v7xKuwjOS9VSJf0rowcj4cY2PAXDElOfXgxHbZF9J/YPeLP0XmG7BAGQSjb6pMTOe0pINeQ513g7I9Rrrm5u0bbVSgpVgJo3GQa61R91TJGV+yM48jpn8TE2bTndsJ2L1KKpQfkzULt7OHMqAuZG73fYAPVNpdafG3SSAVQEo8Jg/jeE509430a+VJQl2SxsPAiy0uBe2wge4Mh9n2TVsL40Yw==',
}

diebasis_user{ 'markus':
  username => 'markus',
  uid => '10002',
  ssh_key => 'AAAAB3NzaC1yc2EAAAABJQAAAQEA8Pu4ytxcl7nEvXxFRNBLVXiA1NCsqAUexhuz/0nnU6vzWDFzAhOuUp8SBfQWFWYHqNK49rOcRDoy7eO/LYEa9iXNS/BF+Mi8sbkatbxiFCTMHebZ/ESvJXbnNguvS4qsa2Mn5O1RuErTnqSs5Qo+uj5XNRm0D3lRahksQPZXG6UNlc++WXhR+8ROmV5lcd/A0HaXKjmQ/ErCmC0DCllEGAraefKsA5dNk58lKXmIj5ftQB9mHM1MU93zsLcxE7ZSMIgQTEsK7hHBWhOOWhRe9iZ5/OYzjRf+AU3LORIoN8DBVdLVIxhazufy6SuI7gniJ+miRZwV4cXbLZT5auJ1OQ==',
}

#diebasis_user{ 'aleksander':
#  username => 'aleksander',
#  uid => '10003',
#  ssh_key => 'AAAAC3NzaC1lZDI1NTE5AAAAIESC3zFc7e+QmoDJPVnQLNO5I4xwIfq3F3ZQY2xoSfPo',
#  ssh_type => 'ssh-ed25519',
#  ssh_title => 'aleks@Cent-HB',
#}
