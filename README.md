Erste Schritte für die Einrichtung eines neuen Debian Servers für die Basis:

1. apt update
2. apt install puppet git
3. git clone https://salsa.debian.org/thk/diebasis-puppet.git
4. vi diebasis-puppet/users.pp # Eigenen Benutzer hinzufügen mit uid zw. 1000 und 9999
5. puppet apply diebasis-puppet
6. vi /etc/dma/auth.conf # Mailjet credentials eintragen

Nach Abschluß der obigen Schritte ist der Server folgendermaßen konfiguriert:

- Der root-user hat kein Passwort mehr und kann auch nicht einloggen.
- SSH-Login per Passwort ist nicht mehr möglich.
- Es wurden Standardbenutzer mit ihren SSH-Keys, ohne Passwort aber mit passwortlosem sudo angelegt.
- Automatische Sicherheitsupdates
- Mailversand, z.B. von Cronjobs

TODO:

- Diese Puppet-Manifests wurden von jemandem angelegt, der gerade erst Puppet lernt.
  Sie entsprechen keiner Best-Practice.
- Es gibt noch keine Verwaltung für Geheimnisse, z.B. dem Mailjet-Passwort
- Von hier aus sollten weitere Installationsschritte definiert werden.
- Idealerweise würden die Benutzer nicht im Manifest definiert werden sondern im LDAP.

Aber besser als gar nichts!

Kontakt: thomas@koch.ro
