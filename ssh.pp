file { '/etc/apt/preferences.d/99puppet-pin-ssh':
  content => "
Package: openssh-server openssh-client openssh-sftp-server
Pin: version 1:8*
Pin-Priority: 991
"
}

# Only version 1:8 and later on Debian includes .d config dirs
package { 'openssh-server':
  ensure => 'latest',
}

file { '/etc/ssh/sshd_config.d/90puppet.conf':
  content => "# File managed by puppet
PasswordAuthentication no
PermitRootLogin no

# Taken from Debian standard config
ChallengeResponseAuthentication no
UsePAM yes
AcceptEnv LANG LC_*
",
  notify => Service['ssh'],
}

file { '/etc/ssh/sshd_config':
  ensure => present,
  content => 'Include /etc/ssh/sshd_config.d/*.conf',
  notify => Service['ssh'],
}

service { 'ssh':
  ensure => running,
  enable => true,
}
