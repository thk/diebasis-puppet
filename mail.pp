# Allow local mails to leave the system
# TODO Die Datei /etc/dma/auth.conf muss manuell konfiguriert werden,
#      weil sie ein Passwort enthaelt

package { 'dma':
  ensure => installed,
}

package { 'bsd-mailx':
  ensure => installed,
}

file { '/etc/aliases':
  content => "*: it@diebasis-partei.de
",
}

file { '/etc/dma/dma.conf':
  content => "# File managed by puppet, do not edit!
SECURETRANSFER
STARTTLS
FULLBOUNCE
MASQUERADE noreply@diebasis-partei.de
MAILNAME ${::fqdn}
"
}

# ::fqdn resolves to the alias defined in /etc/hosts for 127.0.0.1
